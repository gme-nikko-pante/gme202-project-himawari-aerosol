# README Texts from Himawari FTP specific to Aerosol Property

## Aerosol Property (day-time only)
 Latest version: Version 2.1 (Level 2), Version 3.0 (Level 3)
 Observation area: Full-disk
 Temporal resolution: 10-minutes (Level 2),1-hour (Level 3), 
                      1-day (Level 3), 1-month (Level 3)
 Spatial resolution: 5km (Pixel number: 2401, Line number: 2401)
 NOTE: Angstrom exponent included this product is under validation. 
       Users should keep in mind that the data is NOT quality assured. 

# Structure of FTP Directories

## Level 2 (every 10 minutes)
### Aerosol Property (ARP) Level 2
```
 /pub/himawari
       +---/L2
             +---/ARP
                   +---/[VER]
                          +---/[YYYYMM]
                                 +---/[DD]
                                        +---/[hh]
```

## Level 3 (hourly, daily, monthly)
### Aerosol Property Level 3
```
 /pub/himawari
       +---/L3
             +---/ARP
                   +---/[VER]
                          +---/[YYYYMM]
                                 +---/[DD]
                                 +---/[daily]
                                 +---/[monthly]
```



# File Naming Convention

## Level 2
### Aerosol Property
```
 NC_H08_YYYYMMDD_hhmm_L2ARPVER_FLDK.xxxxx_yyyyy.nc

 where YYYY: 4-digit year of observation start time (timeline);
       MM: 2-digit month of timeline;
       DD: 2-digit day of timeline;
       hh: 2-digit hour of timeline;
       mm: 2-digit minutes of timeline;
       VER: version;
       xxxxx: pixel number; and
       yyyyy: line number.

 Example: H08_20150727_0800_L2ARP001_FLDK.02401_02401.nc
```

## Level 3
### Aerosol Property
```
 H08_YYYYMMDD_hhmm_LL_ARPVER_FLDK.xxxxx_yyyyy.nc (5km)

 where YYYY: 4-digit year of observation start time (timeline);
       MM: 2-digit month of timeline;
       DD: 2-digit day of timeline;
       hh: 2-digit hour of timeline;
       mm: 2-digit minutes of timeline;
       LL: 2-digit temporal resolution (L3:hourly, 1D:daily, 1M:monthly) 
       VER: algorithm version; 
       xxxxx: pixel number; and
       yyyyy: line number.

 Example: H08_20150727_0800_1H_ARP001_FLDK.02401_02401.nc
```


# Format

  All data except for Wild Fire is in NetCDF4 format and compressed with gzip.
  Please note that NetCDF format of SST (except monthly product) follows
  the GHRSST Data Specification (GDS) 2.0. Details of GDS2.0, see the Group of 
  High Resolution Sea Surface Temperature (GHRSST) web site
  (https://www.ghrsst.org/). 

  File format of Wild Fire product is CSV. Please see following file
  (H8_WLF_format.txt) for more details. 


# Documents

## Operational schedule of Himawari-8
 Operational schedule of the geostationary Himawari-8 is available from
 the JMA's web site;
 http://www.data.jma.go.jp/mscweb/en/operation8/bulletin_list_H8.html

# Timetable of Himawari-8 Imaging
 The Himawari-8 Imaging Schedule is available from the JMA's web site.
 Please note that no observations are planned at 0240-0250UTC and 1440-1450UTC
 everyday for house-keeping of the Himawai-8 satellite.
 http://www.data.jma.go.jp/mscweb/en/operation8/Himawari-8%20Imaging%20Schedule.pdf


# Model Outputs
 
## Aerosol Property by MRI/JMA
 Latest version: Beta Version
 Area: Global
 Temporal resolution: 1-hour (Level 4)
 Spatial resolution: Longitude 0.375 deg., Latitude 0.37147 to 0.37461 deg. (Gaussian)
 (Pixel number: 960, Line number: 480)
 NOTE: This product is the forecast (every one hour) of aerosol properties by
       the MRI/JMA global aerosol model called Model of Aerosol Species IN the Global
       AtmospheRe (MASINGAR). This product is assimilated by Himawari L3 aerosol
       optical depth at 00, 03, 06, and 09UTC. The opposite side of the Himawari
       observation area is assimilated at 12 and 18UTC using MODIS/Terra+Aqua L3
       Value-added Aerosol Optical Depth - NRT dataset due to lack of aerosol
       retrievals by Himawari.(As for the image on the top page, there are cases
       where preliminary forecast is displayed that was derived by assimilating
       observation data before the previous day.)
       Please refer to the reference below for the assimilation method etc. 
       The aerosol data assimilation system based on MASINGER was developed 
       by Meteorological Research Institute and Kyushu University. 
       The products are produced at Meteorological Research Institute, and provided 
       by JAXA P-Tree System, Japan Aerospace Exploration Agency (JAXA). 

 Acknowledgements: 
    MODIS/Terra+Aqua L3 Value-added Aerosol Optical Depth - NRT datasets
    were acquired from the Level-1 and Atmosphere Archive & Distribution System
    (LAADS) Distributed Active Archive Center (DAAC), located in the Goddard Space
    Flight Center in Greenbelt, Maryland (https://ladsweb.nascom.nasa.gov/).


# Structure of FTP Directories

### Level 4 (model parameters) 
### Aerosol Property Level 4
```
 /pub/model
        +---/ARP
             +---/MS
                   +---/[VER]
                          +---/[YYYYMM]
                                 +---/[DD]

 where VER: algorithm version;
       YYYY: 4-digit year observation start time (timeline);
       MM: 2-digit month of timeline;
       DD: 2-digit day of timeline; and
       hh: 2-digit hour of timeline.
```



# File Naming Convention

## Level 4 (model parameters) 
### Aerosol Property
```
 H08_YYYYMMDD_hhmm_MSARPVER_ANL.xxxxx_yyyyy.nc

 where YYYY: 4-digit year of observation start time (timeline);
       MM: 2-digit month of timeline;
       DD: 2-digit day of timeline;
       hh: 2-digit hour of timeline;
       mm: 2-gidit minutes of timeline;
       VER: algorithm version;
       xxxxx: pixel number; and
       yyyyy: line number.

 Example:  H08_20180727_0000_MSARPbet_ANL.00960_00480.nc
```


# Project

## Source Code

### Installation
Download Anaconda: https://www.anaconda.com/distribution/#download-section
```
conda create --name aot-project-x python=3.7 pip
conda activate aot-project-x
conda install -c anaconda jupyter hdf5 gdal rasterio pyproj netcdf4
jupyter notebook --ip=0.0.0.0 --port=8080
```

## Resources

### Using netCDF4 in Python

https://unidata.github.io/netcdf4-python/netCDF4/index.html
